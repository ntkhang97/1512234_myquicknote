﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.IO;
using PieControls;

namespace MyQuickNote
{
    public class WindowViewModel : ObservableObject
    {
        #region Private members
        #region Custome window
        private Window mWindow;

        /// <summary>
        /// Bề dày của phần kéo thả chỉnh sửa kích thước.
        /// </summary>
        private int ResizeThicknessValue { get; set; } = 5;

        /// <summary>
        /// Giá trị của padding
        /// </summary>
        private int PaddingThicknessValue { get { return 10; } }

        /// <summary>
        /// Chiều rộng của thanh tiêu đề
        /// </summary>
        private int TitleHeightValue { get; set; } = 35;

        /// <summary>
        /// Bề cong của góc màn hình
        /// </summary>
        private int CornerRadiusValue { get { return mWindow.WindowState == WindowState.Maximized ? 0 : 4; } }

        private string TitleName { get; set; } = "MY NOTE";

        /// <summary>
        /// Be day cua phan noi dung
        /// </summary>
        private int InnerContentThicknessValue { get; set; } = 2;
        #endregion

        #region Content of window 
        /// <summary>
        /// Đường dẫn đến data của note
        /// </summary>
        private string NotePath { get { return "Notes.ntk"; } }

        /// <summary>
        /// Đường dẫn đến data của tag
        /// </summary>
        private string TagPath { get { return "Tags.ntk"; } }

        /// <summary>
        /// Luu noi dung cua tag duoc chon tren UI
        /// </summary>
        private string TagSelected;

        /// <summary>
        /// Luu noi dung cua note duoc chon tren UI
        /// </summary>
        private string NoteSelected;

        /// <summary>
        /// Kiem tra menu con co mo hay khong
        /// </summary>
        private bool IsSubMenuNoteOpen = false;

        /// <summary>
        /// Kiem tra menu note co mo hay khong
        /// </summary>
        private bool IsNoteMenuOpen = false;

        /// <summary>
        /// Kiem tra menu tag co mo hay khong
        /// </summary>
        private bool IsTagMenuOpen = false;

        /// <summary>
        /// Danh sách các tag
        /// </summary>
        private string listTag;

        /// <summary>
        /// Tiêu đề của note
        /// </summary>
        private string noteTitle;

        /// <summary>
        /// Nội dung của note
        /// </summary>
        private string noteContent;

        #endregion
        #endregion

        #region Private Methods
        /// <summary>
        /// Load du lieu cho note
        /// </summary>
        private void LoadNoteData()
        {
            try
            {
                if (File.Exists(NotePath)==false)
                    File.Create(NotePath);
                StreamReader sr = new StreamReader(NotePath);

                // Lay so luong note
                int count = Int32.Parse(sr.ReadLine());

                for (int i = 0; i < count; i++)
                {
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();

                    string[] taglist = sr.ReadLine().Split(new string[] { ";" }, StringSplitOptions.None);
                    ObservableCollection<string> taglist_o = new ObservableCollection<string>();
                    foreach (string item in taglist)
                    {
                        taglist_o.Add(item);
                    }

                    AllNote.Add(new Note(title, content, taglist_o));
                }
            }
            catch
            {
                //MessageBox.Show("Cannot read note data");
            }
        }

        /// <summary>
        /// Load du lieu cho tag
        /// </summary>
        private void LoadTagData()
        {
            try
            {
                if (File.Exists(TagPath)==false)
                    File.Create(TagPath);
                using (StreamReader sr = new StreamReader(TagPath))
                // So luong tag
                {
                    int count = Int32.Parse(sr.ReadLine());

                    for (int i = 0; i < count; i++)
                    {
                        string name = sr.ReadLine();
                        int countNote = Int32.Parse(sr.ReadLine());
                        string[] ltnote = sr.ReadLine().Split(new string[] { ";" }, StringSplitOptions.None);
                        AllTag.Add(new Tag(name));
                        AllTag[AllTag.Count - 1].Count = countNote;

                        ObservableCollection<string> ltnote_o = new ObservableCollection<string>();
                        foreach (string item in ltnote)
                        {
                            ltnote_o.Add(item);
                        }
                        AllTag[AllTag.Count - 1].NoteList = ltnote_o;
                    }
                }
            }
            catch
            {
               // MessageBox.Show("Cannot load tag note");
            }
        }

        /// <summary>
        /// Write du lieu cua note
        /// </summary>
        private void WriteNoteData()
        {
            try
            {
                if (File.Exists(NotePath))
                    File.Delete(NotePath);

                using (StreamWriter sw = new StreamWriter(NotePath,true, Encoding.UTF8))
                {
                    // Write so luong note.
                    sw.WriteLine(AllNote.Count);

                    // Write noi dung tung note
                    for (int i = 0; i < AllNote.Count; i++)
                    {
                        // Luu tieu de
                        sw.WriteLine(AllNote[i].Title);

                        // Luu noi dung
                        sw.WriteLine(AllNote[i].Content);

                        // Luu danh sach tag
                        string tl = "";
                        int j = 0;
                        for (; j < AllNote[i].TagList.Count - 1; j++)
                            tl += AllNote[i].TagList[j] + ";";
                        tl += AllNote[i].TagList[j];
                        sw.WriteLine(tl);
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Write du lieu cua tag
        /// </summary>       
        private void WriteTagData()
        {
            try
            {
                if (File.Exists(TagPath))
                    File.Delete(TagPath);

                using (StreamWriter sw = new StreamWriter(TagPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(AllTag.Count);

                    for (int i = 0; i < AllTag.Count; i++)
                    {
                        sw.WriteLine(AllTag[i].Name);
                        sw.WriteLine(AllTag[i].Count);

                        // Luu danh sach note
                        string tl = "";
                        int j = 0;
                        for (; j < AllTag[i].NoteList.Count - 1; j++)
                            tl += AllTag[i].NoteList[j] + ";";
                        tl += AllTag[i].NoteList[j];
                        sw.WriteLine(tl);
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Kiem tra note da ton tai hay chua (note da ton tai neu nhu co cung danh sach tag va cung tieu de
        /// </summary>
        /// <param name="note">Note duoc nhap vao</param>
        /// <returns></returns>
        private int IsNoteExist(Note note)
        {
            for (int i = 0; i < AllNote.Count; i++)
            {
                //// Kiem tra tieu de cua note i
                //if (AllNote[i].Title == note.Title && (AllNote[i].TagList.Count == note.TagList.Count))
                //{
                //    // Kiem tra danh sach tag
                //    bool isExist = true;
                //    for(int j = 0; j < AllNote[i].TagList.Count; j++)
                //    {
                //        if (AllNote[i].TagList[j] != note.TagList[j])
                //        {
                //            isExist = false;
                //            break;
                //        }
                //    }
                //    if(isExist== true)
                //    return i;
                //}
                if (AllNote[i].Title == note.Title)
                    return i;

            }
            return -1;
        }

        /// <summary>
        /// Xoa mot note
        /// </summary>
        /// <param name="note">note can xoa</param>
        private bool DeleteNote(string title)
        {
            int index = -1;
            for (int i = 0; i < AllNote.Count; i++)
                if (AllNote[i].Title == title)
                    index = i;
            if (index == -1)
                return false;
            Note note = AllNote[index];
            // Xoa thong tin cua note trong cac tag lien quan
            for (int i = 0; i < note.TagList.Count; i++)
            {
                for (int k = 0; k < AllTag.Count; k++)
                    if (note.TagList[i] == AllTag[k].Name)
                    {
                        AllTag[k].NoteList.Remove(note.Title);
                        AllTag[k].Count--;
                    }
            }
            AllNote.Remove(note);
            return true;
        }

        #endregion

        #region Public members
        #region CustomWindow
        public double WindowMinHeight { get; set; } = 400;
        public double WindowMinWidth { get; set; } = 400;

        public Thickness ResizeBorderThickness { get { return new Thickness(ResizeThicknessValue); } }

        public Thickness PaddingBorderThickness { get { return new Thickness(PaddingThicknessValue); } }

        public Thickness InnerContentThickness { get { return new Thickness(InnerContentThicknessValue); } }

        public GridLength TitleHeightGridLength { get { return new GridLength(TitleHeightValue); } }

        public CornerRadius WindowCornerRadius
        {
            get
            {
                return new CornerRadius(CornerRadiusValue);
            }
        }

        public string MainWindowTitle { get { return TitleName; } }

        public bool IsNoteChecked { get; set; } = false;

        public bool IsTagChecked { get; set; } = false;
        #endregion

        #region Content of Window
        /// <summary>
        /// Chieu rong cua thanh menubar
        /// </summary>
        public double MenuBarHeight { get; set; } = 40;

        /// <summary>
        /// Danh sách các tag
        /// </summary>
        public string ListTag { get { return listTag; } set { listTag = value; } }

        /// <summary>
        /// Tiêu đề của note
        /// </summary>
        public string NoteTitle { get { return noteTitle; } set { noteTitle = value; } }

        /// <summary>
        /// Nội dung của note
        /// </summary>
        public string NoteContent { get { return noteContent; } set { noteContent = value; } }

        /// <summary>
        /// Danh sách tất cả các tag được tạo
        /// </summary>
        public ObservableCollection<Tag> AllTag { get; set; } = new ObservableCollection<Tag>();

        /// <summary>
        /// Danh sách tất cả các note được tạo
        /// </summary>
        public ObservableCollection<Note> AllNote { get; set; } = new ObservableCollection<Note>();

        /// <summary>
        /// Danh sách các note được lấy theo tên tag
        /// </summary>
        public ObservableCollection<Note> AllSubNote { get; set; } = new ObservableCollection<Note>();

        /// <summary>
        /// Ten cua tag dang duoc chon
        /// </summary>
        public string SelectedTagName
        {
            get { return TagSelected; }
            set
            {
                TagSelected = value;
                TagItemClick();
            }
        }

        /// <summary>
        /// Ten cua note dang duoc chon
        /// </summary>
        public string SelectedNoteName
        {
            get { return NoteSelected; }
            set
            {
                NoteSelected = value;
                NoteItemClick();
            }
        }

        /// <summary>
        /// Danh sach cac phan cua chart
        /// </summary>
        public ObservableCollection<PieSegment> ListPS { get; set; } = new ObservableCollection<PieSegment>();

        /// <summary>
        /// Danh sach cac mau
        /// </summary>
        public List<Color> ListColor = new List<Color>();
        #endregion
        #endregion

        #region Public Commands
        public ICommand MinimizeWindowCommand { get; set; }
        public ICommand MaximizeWindowCommand { get; set; }
        public ICommand CloseWindowCommand { get; set; }
        public ICommand NoteClickCommand { get; set; }
        public ICommand TagClickCommand { get; set; }
        public ICommand SaveClickCommand { get; set; }
        public ICommand NewNoteClickCommand { get; set; }
        public ICommand DeleteNoteClickCommand { get; set; }
        public ICommand PieChartClickCommand { get; set; }
        #endregion

        #region Public Methods

        /// <summary>
        /// Xu ly su kien khi bam vao nut note tren menubar
        /// </summary>
        /// <param name="isChecked">Kiem tra xem nut do co duoc check hay khong</param>
        public void NoteClickEvent(object isChecked)
        {
            IsNoteChecked = (bool)isChecked;

            if (IsNoteChecked == true)
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("NoteExpandMenu");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("NoteExpandMenu_Opacity");
                sb1.Begin();
                sb2.Begin();
                IsNoteMenuOpen = true;
            }
            else
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("NoteCloseMenu");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("NoteCloseMenu_Opacity");
                sb1.Begin();
                sb2.Begin();
                IsNoteMenuOpen = false;
            }
        }

        /// <summary>
        /// Xu ly su kien khi bam vao nut tag tren menubar
        /// </summary>
        /// <param name="isChecked"></param>
        public void TagClickEvent(object isChecked)
        {
            IsTagChecked = (bool)isChecked;

            if (IsTagChecked == true)
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("TagExpandMenu");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("TagExpandMenu_Opacity");
                sb1.Begin();
                sb2.Begin();
                IsTagMenuOpen = true;
            }
            else
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("TagCloseMenu");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("TagCloseMenu_Opacity");
                if (IsSubMenuNoteOpen == true)
                {
                    Storyboard sb3 = (Storyboard)mWindow.TryFindResource("NoteCloseSubMenu");
                    Storyboard sb4 = (Storyboard)mWindow.TryFindResource("NoteCloseSubMenu_Opacity");
                    sb3.Begin();
                    sb4.Begin();
                    IsSubMenuNoteOpen = false;
                }
                sb1.Begin();
                sb2.Begin();
                IsTagMenuOpen = false;
            }
        }

        /// <summary>
        /// Xử lý sự kiện khi bấm vào nút save
        /// </summary>
        /// <param name="o"></param>
        public void SaveClickEvent(object o)
        {
            #region Xử lý cho tag
            if (ListTag == "" || NoteContent == "")
            {
                MessageBox.Show("Bạn phải gắn tag và tiêu đề cho ghi chú");
                return;
            }
            // Tach dau phay va khoang trang cho tag
            try
            {
                string[] ListTagName = ListTag.Split(new string[] { "," }, StringSplitOptions.None);


                for (int i = 0; i < ListTagName.Length; i++)
                {
                    ListTagName[i] = ListTagName[i].Trim();
                    ListTagName[i] = ListTagName[i].ToUpperInvariant();
                }

                // Kiem tra tag da co chua, neu chua thi tao tag moi
                foreach (string TagName in ListTagName)
                {
                    bool isAppear = false;
                    foreach (string TagNameInTagClass in Tag.TagList)
                        if (TagName == TagNameInTagClass)
                        {
                            isAppear = true;
                            break;
                        }
                    if (isAppear == false)
                        AllTag.Add(new Tag(TagName));
                }
                #endregion

                #region Xử lý cho note

                ObservableCollection<string> ListTagName_O = new ObservableCollection<string>();
                foreach (var item in ListTagName)
                {
                    ListTagName_O.Add((string)item);
                }

                Note temp = new Note(NoteTitle, NoteContent, ListTagName_O);
                int pos = IsNoteExist(temp);
                if (pos != -1)
                    DeleteNote(temp.Title);
                AllNote.Add(temp);

                // Them tieu de cua note vao danh sach cua cac tag ma note do gan the
                foreach (string TagName in ListTagName)
                {
                    for (int i = 0; i < AllTag.Count; i++)
                    {
                        if (TagName == AllTag[i].Name)
                        {
                            AllTag[i].NoteList.Add(NoteTitle);
                            AllTag[i].Count++;
                        }
                    }
                }

                #endregion

                #region Ghi Du Lieu Xuong File
                WriteNoteData();
                WriteTagData();
                #endregion
            }
            catch { };
        }

        /// <summary>
        /// Xử lý sự kiện khi bấm chọn 1 note
        /// </summary>
        public void NoteItemClick()
        {
            // Tim note dang duoc chon trong all note
            int index = -1;
            for (int i = 0; i < AllNote.Count; i++)
            {
                if (AllNote[i].Title == SelectedNoteName)
                {

                    index = i;
                    break;
                }
            }

            if (index == -1)
                return;
            // Lay danh sach cac tag
            string str = "";
            int k = 0;
            for (; k < AllNote[index].TagList.Count - 1; k++)
            {
                str += AllNote[index].TagList[k] + ",";
            }
            str += AllNote[index].TagList[k];
            listTag = str;
            OnPropertyChanged("ListTag");
            // Lay tieu de
            noteTitle = AllNote[index].Title;
            OnPropertyChanged("NoteTitle");
            // Lay noi dung
            noteContent = AllNote[index].Content;
            OnPropertyChanged("NoteContent");
        }

        /// <summary>
        /// Xử lý sự kiện khi chọn tag trên menu tag
        /// </summary>
        public void TagItemClick()
        {
            AllSubNote.Clear();
            // Tim tag co ten dang duoc chon
            int index = -1;
            for (int i = 0; i < AllTag.Count; i++)
            {
                if (AllTag[i].Name == SelectedTagName)
                {
                    index = i;
                    break;
                }
            }
            if (index == -1)
                return;
            // Lay tat ca cac note co trong ds NoteList cua tag do
            for (int i = 0; i < AllNote.Count; i++)
            {
                for (int j = 0; j < AllNote[i].TagList.Count; j++)
                    if (AllNote[i].TagList[j] == AllTag[index].Name)
                    {
                        AllSubNote.Add(AllNote[i]);
                    }
            }

            if (IsSubMenuNoteOpen == false && (IsNoteMenuOpen == true || IsTagMenuOpen == true))
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("NoteExpandSubMenu");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("NoteExpandSubMenu_Opacity");
                sb1.Begin();
                sb2.Begin();
                IsSubMenuNoteOpen = true;
            }
        }

        /// <summary>
        /// Xử lý sự kiện khi bấm tạo note mới
        /// </summary>
        /// <param name="o"></param>
        public void NewNoteClickEvent(object o)
        {
            ListTag = "";
            NoteTitle = "";
            NoteContent = "";
            OnPropertyChanged("ListTag");
            OnPropertyChanged("NoteTitle");
            OnPropertyChanged("NoteContent");
        }

        /// <summary>
        /// Xử lý sự kiện bấm nút xóa
        /// </summary>
        /// <param name="o"></param>
        private void DeleteNoteClickEvent(object o)
        {
            if (DeleteNote(NoteTitle) == true)
            {
                MessageBox.Show("Đã xóa ghi chú:" + NoteTitle);
                NewNoteClickEvent("oke");
                WriteNoteData();
                WriteTagData();
            }
            else
                MessageBox.Show("Không thể xóa");
        }

        public void PieChartClickEvent(object o)
        {
            ListPS.Clear();
            for(int i = 0; i < AllTag.Count; i ++)
            {
                ListPS.Add(new PieSegment { Color = ListColor[i % 7], Name = AllTag[i].Name, Value = AllTag[i].Count });
            }

            PieChart chart1 = (PieChart)mWindow.FindName("chart1");
           
            chart1.Data = ListPS;

            if ((bool)o == true)
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("NoteExpandPieChart");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("NoteExpandPieChart_Opacity");
                sb1.Begin();
                sb2.Begin();
            }
            else
            {
                Storyboard sb1 = (Storyboard)mWindow.TryFindResource("NoteClosePieChart");
                Storyboard sb2 = (Storyboard)mWindow.TryFindResource("NoteClosePieChart_Opacity");
                sb1.Begin();
                sb2.Begin();
            }

        }
        #endregion

        #region Constructor
        public WindowViewModel(Window Wnd)
        {
            mWindow = Wnd;

            ListColor.Add(Colors.Red);
            ListColor.Add(Colors.Orange);
            ListColor.Add(Colors.Yellow);
            ListColor.Add(Colors.Gray);
            ListColor.Add(Colors.Blue);
            ListColor.Add(Colors.SkyBlue);
            ListColor.Add(Colors.Violet);

            mWindow.StateChanged += (sender, e) =>
              {
                  OnPropertyChanged("ResizeBorderThickness");
                  OnPropertyChanged("PaddingBorderThickness");
                  OnPropertyChanged("TitleHeightGridLength");
                  OnPropertyChanged("WindowCornerRadius");
                  OnPropertyChanged("IsNoteChecked");
                  OnPropertyChanged("IsTagChecked");
                  OnPropertyChanged("ListTag");
                  OnPropertyChanged("NoteTitle");
                  OnPropertyChanged("NoteContent");
              };

            LoadNoteData();

            LoadTagData();

            #region Dinh nghia cac lenh cho nut tat va thu nho
            //if (mWindow.WindowState != WindowState.Maximized)
            //    MaximizeWindowCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Maximized);
            MinimizeWindowCommand = new RelayCommand((o) => mWindow.WindowState = WindowState.Minimized);
            CloseWindowCommand = new RelayCommand((o) => mWindow.Close());
            #endregion

            #region Dinh nghia cac lenh cho nut tren menubar
            NoteClickCommand = new RelayCommand(NoteClickEvent);
            TagClickCommand = new RelayCommand(TagClickEvent);
            SaveClickCommand = new RelayCommand(SaveClickEvent);
            NewNoteClickCommand = new RelayCommand(NewNoteClickEvent);
            DeleteNoteClickCommand = new RelayCommand(DeleteNoteClickEvent);
            PieChartClickCommand = new RelayCommand(PieChartClickEvent);
            #endregion
        }
        #endregion
    }
}
