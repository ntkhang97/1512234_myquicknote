﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace MyQuickNote
{
    /// <summary>
    /// Cau truc cua mot note
    /// </summary>
    public class Note
    {
        /// <summary>
        /// Tiêu đề của note
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Nội dung của note
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Danh sách các tag của note
        /// </summary>
        public ObservableCollection<string> TagList { get; set; } = new ObservableCollection<string>();

        /// <summary>
        /// Tạo một ghi chú
        /// </summary>
        /// <param name="title">Tiêu đề của ghi chú</param>
        /// <param name="content">Nội dung của ghi chú</param>
        /// <param name="tagList">Danh sách các tag</param>
        public Note(string title, string content, ObservableCollection<string> tagList)
        {
            Title = title;
            Content = content;
            TagList = tagList;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
