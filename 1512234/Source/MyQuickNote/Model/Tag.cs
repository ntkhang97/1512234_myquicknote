﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQuickNote
{
    public class Tag : ObservableObject
    {
        /// <summary>
        /// Tên của tag
        /// </summary>
        public string Name { get; set; }

        private int count = 0;

        /// <summary>
        /// Số lượng note của tag
        /// </summary>
        public int Count
        {
            get { return count; }
            set { count = value; OnPropertyChanged("Count"); }
        }

        /// <summary>
        /// Danh sách tên các ghi chú
        /// </summary>
        public ObservableCollection<string> NoteList { get; set; } = new ObservableCollection<string>();

        /// <summary>
        /// Danh sách tên của tất cả các tag
        /// </summary>
        public static HashSet<string> TagList { get; set; } = new HashSet<string>();

        /// <summary>
        /// Tạo một tag
        /// </summary>
        /// <param name="name">Tên của tag</param>
        public Tag(string name)
        {
            Name = name;
            if(name!=null)
            TagList.Add(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
